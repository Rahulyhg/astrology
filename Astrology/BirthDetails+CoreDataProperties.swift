//
//  BirthDetails+CoreDataProperties.swift
//  Astrology
//
//  Created by Rishabh Wadhwa on 30/12/15.
//  Copyright © 2015 Sunpac Solutions. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension BirthDetails {

    @NSManaged var birthDate: String?
    @NSManaged var birthPlace: String?
    @NSManaged var birthTime: String?

}
