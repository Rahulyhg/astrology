//
//  ProductViewController.swift
//  Astrology
//
//  Created by Rishabh Wadhwa on 22/12/15.
//  Copyright © 2015 Sunpac Solutions. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //To get all the product details
    let dataController = DataController.sharedInstance
    
    var selectedProduct:Int!
    var selectedCategory:Int!
    
    //To set the first image as default image
    var selectedPreview = 0
    var previousPage = "category"
    
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var verticalScrollView: UIScrollView!
    @IBOutlet weak var maxPrice: UILabel!
    @IBOutlet weak var finalPrice: UILabel!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var stockAvailability: UILabel!
    @IBOutlet weak var ratingStar1: UIImageView!
    @IBOutlet weak var ratingStar2: UIImageView!
    @IBOutlet weak var ratingStar3: UIImageView!
    @IBOutlet weak var ratingStar4: UIImageView!
    @IBOutlet weak var ratingStar5: UIImageView!
    @IBOutlet weak var descriptionProduct: UITextView!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var previewImageCollection: UICollectionView!
    @IBOutlet weak var headerMenu: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set title and capitalize title
        setTitle()
        
        //Set title and prices of product and capitalize title
        setProductImage()
        
        //Set Prices of product and calculate discount
        setPricing()
        
        //Check if product quantity is 0 then change stockAvailability to Out of Stock and disable buy now/add to cart buttons
        checkStock()
        
        //Set rating stars
        setRating()
        
        //Set product description and set its height accordingly
        setProductDescription()
        
        //Add shadow to main header
        self.addShadowToHeader()
    }
    
    //Set title and capitalize title
    func setTitle(){
        productTitle.text = dataController.productName[selectedProduct]
        productTitle.text = productTitle.text!.uppercaseString
    }
    
    //Set product image when preview is click or set it as default in beginning
    func setProductImage() {
        productImage.image = UIImage(named: dataController.productImages[selectedProduct][selectedPreview])
    }
    
    //Set no of preview images
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let totalImages = dataController.productImages[selectedProduct].count
        print("Total no of cells are \(totalImages)")
        return totalImages
    }
    
    //Set image for preview image
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let previewImageCell = collectionView.dequeueReusableCellWithReuseIdentifier("previewImage", forIndexPath: indexPath)
        
        let previewImageButton = UIButton(frame: CGRectMake(0, 0, 100, 100))
        let previewImage = UIImageView(frame: CGRectMake(0, 0, 100, 100))
        previewImage.image = UIImage(named: dataController.productImages[selectedProduct][indexPath.row])
        if(indexPath.row == selectedPreview) {
            previewImageCell.layer.borderColor = UIColor(red: 0.97, green: 0.58, blue: 0.12, alpha: 1.0).CGColor
        }
        else {
            previewImageCell.layer.borderColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0).CGColor
        }
        previewImageCell.layer.borderWidth = 1
        previewImageButton.addSubview(previewImage)
        
        previewImageButton.tag = indexPath.row
        previewImageButton.addTarget(self, action: "selectPreviewImage:", forControlEvents: .TouchUpInside)
        
        previewImageCell.addSubview(previewImageButton)
        return previewImageCell
    }
    
    //Change product image from preview image selection and set border accordingly
    func selectPreviewImage(sender: UIButton!) {
        let oldSelection = selectedPreview
        selectedPreview = sender.tag

        print("Old selection is \(oldSelection)")
        print("New selection is \(selectedPreview)")
    
        //Change borders of new and previous cell according to selection
        let previousCell = previewImageCollection.cellForItemAtIndexPath(NSIndexPath(forRow: oldSelection, inSection: 0))
        let selectedCell = previewImageCollection.cellForItemAtIndexPath(NSIndexPath(forRow: selectedPreview, inSection: 0))
        previousCell?.layer.borderColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0).CGColor
        selectedCell?.layer.borderColor = UIColor(red: 0.97, green: 0.58, blue: 0.12, alpha: 1.0).CGColor
        
        setProductImage()
    }
    
    //Set Prices of product and calculate discount
    func setPricing() {
        let attrString = NSAttributedString(string: dataController.currency + "\(dataController.productOldPrice[selectedProduct])/-", attributes: [NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue])
        maxPrice.attributedText = attrString
        finalPrice.text = dataController.currency + String(dataController.productNewPrice[selectedProduct]) + "/-"
        
        let discountPercent:Int! = Int(((CGFloat(dataController.productNewPrice[selectedProduct])-CGFloat(dataController.productOldPrice[selectedProduct]))/CGFloat(dataController.productOldPrice[selectedProduct]))*CGFloat(100))
        discount.text = "\(discountPercent)" + "%"
        discount.layer.cornerRadius = 2
        discount.layer.masksToBounds = true
    }
    
    //Check if product quantity is 0 then change stockAvailability to Out of Stock and disable buy now/add to cart buttons
    func checkStock() {
        if(dataController.productQuantity[selectedProduct]==0) {
            stockAvailability.text = "Out of Stock"
            stockAvailability.textColor = UIColor(red: 1.0, green: 0, blue: 0, alpha: 1.0)
            addToCartButton.enabled = false
            addToCartButton.backgroundColor =  UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 0.3)
            buyNowButton.backgroundColor =  UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 0.3)
            buyNowButton.enabled = false
        }
    }
    
    //Set product description and set its height accordingly
    func setProductDescription() {
        descriptionProduct.text = dataController.productDescription[selectedProduct]
        descriptionProduct.sizeToFit()
    }
    
    //Set rating stars
    func setRating() {
        switch dataController.productRating[selectedProduct] {
        case 5:
            ratingStar5.highlighted = true
            ratingStar4.highlighted = true
            ratingStar3.highlighted = true
            ratingStar2.highlighted = true
            ratingStar1.highlighted = true
            print("Rating 5")
        case 4:
            ratingStar5.highlighted = false
            ratingStar4.highlighted = true
            ratingStar3.highlighted = true
            ratingStar2.highlighted = true
            ratingStar1.highlighted = true
            print("Rating 4")
        case 3:
            ratingStar5.highlighted = false
            ratingStar4.highlighted = false
            ratingStar3.highlighted = true
            ratingStar2.highlighted = true
            ratingStar1.highlighted = true
            print("Rating 3")
        case 2:
            ratingStar5.highlighted = false
            ratingStar4.highlighted = false
            ratingStar3.highlighted = false
            ratingStar2.highlighted = true
            ratingStar1.highlighted = true
            print("Rating 2")
        case 1:
            ratingStar5.highlighted = false
            ratingStar4.highlighted = false
            ratingStar3.highlighted = false
            ratingStar2.highlighted = false
            ratingStar1.highlighted = true
            print("Rating 1")
        default:
            ratingStar5.highlighted = false
            ratingStar4.highlighted = false
            ratingStar3.highlighted = false
            ratingStar2.highlighted = false
            ratingStar1.highlighted = false
            print("Rating 0")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //To set the category of product and send it when back button is pressed
    @IBAction func backButton(sender: AnyObject) {
        if(previousPage == "category") {
            performSegueWithIdentifier("productToCategoryClick", sender: self)
        }
        else {
            performSegueWithIdentifier("productToBuyClick", sender: self)
        }

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // set category when clicked to change category
        if (segue.identifier == "productToCategoryClick") {
            let nav = segue.destinationViewController as! UINavigationController
            let svc = nav.topViewController as! CategoryViewController
            svc.selectedCategory = selectedCategory
        }
        if (segue.identifier == "productToBuyClick") {
            let nav = segue.destinationViewController as! MenuViewController
            nav.firstScreen = "buy"
        }
    }
    
    //Change status bar text to white
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    //Add shadow to main header
    func addShadowToHeader() {
        self.headerMenu.layer.shadowColor = UIColor(white: 0.3, alpha: 1.0).CGColor
        self.headerMenu.layer.shadowOffset = CGSizeMake(0, 1)
        self.headerMenu.layer.shadowOpacity = 0.7
        self.headerMenu.layer.shadowRadius = 0.3
        self.headerMenu.layer.masksToBounds = false
    }
}
