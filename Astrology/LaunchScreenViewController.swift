//
//  LaunchScreenViewController.swift
//  Astrology Science
//
//  Created by Rishabh Wadhwa on 09/02/16.
//  Copyright © 2016 Sunpac Solutions. All rights reserved.
//

import UIKit
import CoreData

class LaunchScreenViewController: UIViewController {

    @IBOutlet weak var sunView: UIImageView!
    @IBOutlet weak var mercuryView: UIImageView!
    @IBOutlet weak var venusView: UIImageView!
    @IBOutlet weak var earthView: UIImageView!
    @IBOutlet weak var marsView: UIImageView!
    
    // Retreive the managedObjectContext from AppDelegate
    let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    //To get all the product details
    let dataController = DataController.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(animated: Bool) {
        UIView.animateWithDuration(6, animations: {
            self.mercuryView.center.x = self.mercuryView.center.x + 10
            self.mercuryView.center.y = self.mercuryView.center.y - 10
            
            self.venusView.center.x = self.venusView.center.x + 20
            self.venusView.center.y = self.venusView.center.y - 5
            
            self.earthView.center.x = self.earthView.center.x + 22
            self.earthView.center.y = self.earthView.center.y - 8
            
            self.marsView.center.x = self.marsView.center.x + 25
            self.marsView.center.y = self.marsView.center.y - 10

        },
        completion: { finished in
            self.fetchBirthDetails()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Fetch birth details to check which view to load next
    func fetchBirthDetails() {
        let fetchRequest = NSFetchRequest(entityName: "BirthDetails")
        
        //Fetch the birth details
        do {
            let results = try self.managedContext.executeFetchRequest(fetchRequest)
            print("Birth details fetched are \(results)")
            let birthDetail = results as! [BirthDetails]
            
            if(birthDetail != []) {
                dataController.birthDate = birthDetail[0].birthDate!
                dataController.birthTime = birthDetail[0].birthTime!
                dataController.birthPlace = birthDetail[0].birthPlace!
            }
            
            print("Birth details fetched are, Birth date \(dataController.birthDate), Birth time \(dataController.birthTime), Birth Place \(dataController.birthPlace)")
            if (dataController.birthDate != nil && dataController.birthTime != nil && dataController.birthPlace != nil) {
                print("Data found")
                
                dispatch_async(dispatch_get_main_queue()) {
                    [unowned self] in
                    self.performSegueWithIdentifier("launchToHome", sender: self)
                }
            }
            else {
                print("No data found")
                self.performSegueWithIdentifier("launchToBirth", sender: self)
            }
        }
            
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            //performSegueWithIdentifier("dataToBirthDetails", sender: self)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
